![stability-work_in_progress](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

Once most of our bibliographical production were digitize, a step must be fulfilled: bookmarks and inner sections (titles and subtitles) must be added for a proper indexing and meets Google [SEO](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=2&cad=rja&uact=8&ved=0ahUKEwjPvYzl-NDbAhVGHZAKHbncA2IQFgg6MAE&url=https%3A%2F%2Fwww.hildamateiu.com%2Fdocs%2Fsearch-engine-optimization-starter-guide.pdf&usg=AOvVaw1G_1xuwe-LgXHzoInOg8wV) directives. In a nutshell, a good practices to-do to all pdf files

### What is this repository for? ###

* Quick summary
    - Semantize inner structure of every digital document to meet Google's SEO directives
* Version 1.1

### How do I get set up? ###

* Summary of set up
    - _In the making_
* Configuration
    - _In the making_
* Dependencies
    - _In the making_
* Database configuration
    - _In the making_
* How to run tests
    - _In the making_
* Deployment instructions
    - _In the making_
    
### Related repositories ###

* Some repositories linked with this project:
     - [Digitalizacion (worflow)](https://bitbucket.org/imhicihu/digitalizacion-worflow/)
     - [Incunnabilia (early book)- Digitization](https://bitbucket.org/imhicihu/incunnabilia-early-book-digitization/)
     - [Tell el Ghaba digital assembly](https://bitbucket.org/imhicihu/tell-el-ghaba-digital-assembly)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/pdf-inner-structure/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/pdf-inner-structure/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/pdf-inner-structure/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/pdf-inner-structure/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### Licence ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)  