## Technical requirements ##

* Hardware
     - Macbook 13 inches
     - Satechi Type-C Multiport Adapter (hub USB & Network access)
     - _Hosting_: Consorcio Saavedra 15
     - _Bandwidth_: Consorcio Saavedra 15
* Software
     - PDF Jump Marks
          - [Jump](https://jump.moapp.software/)
